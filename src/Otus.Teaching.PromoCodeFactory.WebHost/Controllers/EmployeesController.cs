﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _rolesRepository;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> rolesRepository)
        {
            _employeeRepository = employeeRepository;
            _rolesRepository = rolesRepository;
        }
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Добавить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(EmployeeAddRequest), (int)HttpStatusCode.OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> AddEmployeeAsync(EmployeeAddRequest employeeR)
        {
            List<Role> listR_ = new List<Role>();
            listR_ = CreateListRole(employeeR.Roles);
            var employee = new Employee()
            {
                Id = Guid.NewGuid(),
                FirstName = employeeR.FirstName,
                LastName = employeeR.LastName,
                Email = employeeR.Email,
                Roles = listR_,
                AppliedPromocodesCount = employeeR.AppliedPromocodesCount
            };
            var NewId = await _employeeRepository.Create(employee);

            return Ok("Сотрудник добавлен c ID=" + NewId);

        }

        /// <summary>
        /// корректировка сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        [ProducesResponseType(typeof(Guid), (int)HttpStatusCode.OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Guid>> EditEmployeeAsync(Guid id, EmployeeUpdateRequest employeeR)
        {
            try
            {
                List<Role> listR_ = new List<Role>();
                listR_ = CreateListRole(employeeR.Roles);

                var employee = await _employeeRepository.GetByIdAsync(id);
                if (employee == null)
                    return NotFound(new { result = "error", errorMessage = "Сотрудник не найден!" });

                employee.FirstName = !string.IsNullOrEmpty(employeeR.FirstName) ? employeeR.FirstName : employee.FirstName;
                employee.LastName = !string.IsNullOrEmpty(employeeR.LastName) ? employeeR.LastName : employee.LastName;
                employee.Roles = listR_.Count != 0 ? listR_ : employee.Roles;
                employee.Email = !string.IsNullOrEmpty(employeeR.Email) ? employeeR.Email : employee.Email;
                employee.AppliedPromocodesCount = employeeR.AppliedPromocodesCount != 0 ? employeeR.AppliedPromocodesCount : employee.AppliedPromocodesCount;
                await _employeeRepository.Update(employee);
                return Ok(id);
            }
            catch (Exception ex) { return BadRequest(new { result = "error", errorMessage = $"Обновление не успешно! {ex.Message}" }); }
        }

        /// <summary>
        ///Удаление сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteEmployeesAsync(Guid id)
        {
            try
            {
                var employee = await _employeeRepository.GetByIdAsync(id);
                if (employee == null)
                    return NotFound(new { result = "error", errorMessage = "Сотрудник не найден!" });
                await _employeeRepository.Delete(id);

                return Ok();
            }
            catch (Exception ex) { return BadRequest(new { result = "error", errorMessage = $"Удаление не выполнено! {ex.Message}" }); }
        }
        protected List<Role> CreateListRole(List<Guid> roleID_)
        {
            List<Role> reqList = new List<Role>();
            // ActionResult<RoleItemResponse> rowRole ;
            if (roleID_ != null)
            {
                foreach (var roleId in roleID_)
                {
                    var row = _rolesRepository.GetByIdAsync(roleId);
                    if (row.Result != null)
                    {
                        reqList.Add(new Role()
                        {
                            Id = row.Result.Id,
                            Name = row.Result.Name,
                            Description = row.Result.Description
                        });
                    }
                }
            }
            return reqList;
        }

    }
}