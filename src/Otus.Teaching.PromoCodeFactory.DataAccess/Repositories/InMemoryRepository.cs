﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        //protected IEnumerable<T> Data { get; set; }
        protected List<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data.ToList();
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data.AsEnumerable());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<bool> Find(Guid id)
        {
            return Task.FromResult(Data.Any(x => x.Id == id));
        }

        public Task<Guid> Create(T entity)
        {
            Data.Add(entity);
            return Task.FromResult(entity.Id);

        }

        public Task<Guid> Update(T entity)
        {
            Data = Data.Where(x => x.Id != entity.Id).Append(entity).ToList();
            return Task.FromResult(entity.Id);
        }

        public Task Delete(Guid id)
        {
            Data.RemoveAll(x => x.Id == id);
            return Task.CompletedTask;
        }
    }
}